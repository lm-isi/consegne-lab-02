package u02esLab

import org.junit.Assert.assertTrue
import org.junit.Assert.assertFalse
import org.junit.Test
import Es3.*

class Es3Test {
  val evenNumber = 2
  val isEven: (Int) => Boolean = x => x%2==0
  val isOdd: Int => Boolean = neg(isEven)

  val notEmptyString = "ciao"
  val empty: String => Boolean = _ == ""
  val notEmpty: String => Boolean = neg(empty)

  @Test def testIsOdd(): Unit = assertFalse(isOdd(evenNumber))
  @Test def testIsNotEmptyOdd(): Unit = assertTrue(notEmpty(notEmptyString))
}
