package u02esLab

import org.junit.Assert.{assertEquals, assertFalse, assertTrue}
import org.junit.Test
import scala.math._
import Es7.*

class Es7Test:
  val circleOne: Shape.Circle = Shape.Circle(1)
  val circleTwo: Shape.Circle = Shape.Circle(2)
  val circleThree: Shape.Circle = Shape.Circle(3)

  val rectangleOne: Shape.Rectangle = Shape.Rectangle(1,2)
  val rectangleTwo: Shape.Rectangle = Shape.Rectangle(2,3)
  val rectangleThree: Shape.Rectangle = Shape.Rectangle(3,4)

  val squareOne: Shape.Square = Shape.Square(1)
  val squareTwo: Shape.Square = Shape.Square(2)
  val squareThree: Shape.Square = Shape.Square(3)

  val deltaForAsserts: Double = 0.0001

  @Test def testCirclePerimeter(): Unit =
    assertEquals(2*circleOne.radius*Pi,Es7.perimeter(circleOne),deltaForAsserts)
    assertEquals(2*circleTwo.radius*Pi,Es7.perimeter(circleTwo),deltaForAsserts)
    assertEquals(2*circleThree.radius*Pi,Es7.perimeter(circleThree),deltaForAsserts)

  @Test def testCircleArea(): Unit =
    assertEquals(circleOne.radius*circleOne.radius*Pi,Es7.area(circleOne),deltaForAsserts)
    assertEquals(circleTwo.radius*circleTwo.radius*Pi,Es7.area(circleTwo),deltaForAsserts)
    assertEquals(circleThree.radius*circleThree.radius*Pi,Es7.area(circleThree),deltaForAsserts)

  @Test def testRectanglePerimeter(): Unit =
    assertEquals(2*rectangleOne.length+2*rectangleOne.width, Es7.perimeter(rectangleOne), deltaForAsserts)
    assertEquals(2*rectangleTwo.length+2*rectangleTwo.width, Es7.perimeter(rectangleTwo), deltaForAsserts)
    assertEquals(2*rectangleThree.length+2*rectangleThree.width, Es7.perimeter(rectangleThree), deltaForAsserts)

  @Test def testRectangleArea(): Unit =
    assertEquals(rectangleOne.length*rectangleOne.width, Es7.area(rectangleOne), deltaForAsserts)
    assertEquals(rectangleTwo.length*rectangleTwo.width, Es7.area(rectangleTwo), deltaForAsserts)
    assertEquals(rectangleThree.length*rectangleThree.width, Es7.area(rectangleThree), deltaForAsserts)

  @Test def testSquarePerimeter(): Unit =
    assertEquals(4*squareOne.length, Es7.perimeter(squareOne), deltaForAsserts)
    assertEquals(4*squareTwo.length, Es7.perimeter(squareTwo), deltaForAsserts)
    assertEquals(4*squareThree.length, Es7.perimeter(squareThree), deltaForAsserts)

  @Test def testSquareArea(): Unit =
    assertEquals(squareOne.length*squareOne.length, Es7.area(squareOne), deltaForAsserts)
    assertEquals(squareTwo.length*squareTwo.length, Es7.area(squareTwo), deltaForAsserts)
    assertEquals(squareThree.length*squareThree.length, Es7.area(squareThree), deltaForAsserts)