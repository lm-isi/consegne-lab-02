package u03run

enum Option[T]:
  case Some(t: T)
  case None()

// Companion object dell'enum
// Vogliamo definire alcuni algoritmi sugli Option. Però ci sono molte possibilità su quello che potrebbe succedere
// in base a quello che verrà fornito, infatti Option potrebbe anche essere vuoto.
// Con gli opzionali abbiamo un box con dentro un valore. Idealmente vogliamo utilizzare il valore che è dentro il box
// per poi ritornare un box con un nuovo valore al suo interno.
// I moduli non sono generichi. La genericità la specifico nella definizione del metodo
object Option:

  // def increment(o: Option): Option = o match
  def map[T,O](o: Option[T], f: T => O): Option[O] = o match
    case Some(i) => Some(f(i))
    // piuttosto che "case Some(i) => Some(i+1)" posso rendere increment come high-horder, alla quale passo una funzione
    // per fare l'operazione desiderata
    case None() => None()



object ADT extends App:
  // Le import possono essere in qualsiasi punto ed è visibile solo dentro questo Object.
  import Option.*

  // Sono da vedere come dei valori, e non come oggetti.
  //  val v: Option = Option.Some(5)
  //  val w: Option = Option.None()
  val v = Some(5)
  // Option di None in seguito non viene riconosciuto. Quindi va specificato per poterlo utilizzare nei metodi in seguito
  // val w = None()
  val w = None[Int]()

  // Devo usare Option sia per definire i valori che utilizzare i metodi
  // println(Option.increment(v))
  // println(Option.increment(w))
  println(map(v,_*2))
  println(map(w,_+2))
  // Dopo aver cambiato il tipo di uscita in qualcosa di diverso rispetto l'entrata, posso concatenarci anche una
  // stringa, ad esempio.
  println(map(v,_+"ciao"))