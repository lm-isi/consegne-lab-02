package u02esLab

object Es8 extends App :
  enum Option[A]:
    case Some(a: A)
    case None() // here parens are needed because of genericity

  object Option:

    def isEmpty[A](opt: Option[A]): Boolean = opt match
      case None() => true
      case _ => false

    def orElse[A, B >: A](opt: Option[A], orElse: B): B = opt match
      case Some(a) => a
      case _ => orElse

    def flatMap[A, B](opt: Option[A])(f: A => Option[B]): Option[B] = opt match
      case Some(a) => f(a)
      case _ => None()

    def filter[A](opt: Option[A])(f: A => Boolean): Option[A] = opt match
      case Some(a) if f(a) => Some(a)
      case _ => None()

    def map[A, B](opt: Option[A])(f: A => B): Option[B] = opt match
      case Some(a) => Some(f(a))
      case _ => None()

    def map2[A, B, D](opt1: Option[A], opt2: Option[B]): Option[D] = opt1 match
      case Some(a) => opt2 match
        case Some(b) => Some(a + b)
        case _ => None()
      case _ => None()

    def map2Int(opt1: Option[Int], opt2: Option[Int]): Option[Int] = opt1 match
      case Some(a) => opt2 match
        case Some(b) => Some(a + b)
        case _ => None()
      case _ => None()


  import Option.*

  println(map(Some(5))(_ < 7))
  println(map(Some(5))(_ < 3))
  println(map(None[Int]())(_ < 3))
  println(map(Some(5))(_ + 3))
