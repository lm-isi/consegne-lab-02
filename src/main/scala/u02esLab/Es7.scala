package u02esLab

import scala.math._

object Es7 extends App:
  enum Shape:
    case Circle(radius: Double)
    case Rectangle(length: Double, width: Double)
    case Square(length: Double)

  def perimeter(s: Shape): Double = s match
    case Shape.Circle(r) => r*2*Pi
    case Shape.Rectangle(l,w) => 2*l+2*w
    case Shape.Square(l) => 4*l

  def area(s: Shape): Double = s match
    case Shape.Circle(r) => (r*r)*Pi
    case Shape.Rectangle(l,w) => l*w
    case Shape.Square(l) => l*l
