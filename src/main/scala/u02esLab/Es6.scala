package u02esLab

object Es6 extends App:
  def fib(n: Int): Int = n match
    case 0 => 0
    case 1 => 1
    case _ => fib(n-1) + fib(n-2)
    // Non è una recursion tail dato che per essere completata deve giungere ai valori 0 o 1 per poi sommare tutti
    // quelli precedenti

