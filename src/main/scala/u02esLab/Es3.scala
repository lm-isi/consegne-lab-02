package u02esLab

object Es3 extends App:
  val parity: Int => String = _ match
    case n if n%2 == 0 => "even"
    case _ => "odd"

  def parity_2(x: Int): String = x match
    case n if n%2==0 => "even"
    case _ => "odd"

  def neg[X](f: X => Boolean): X => Boolean = !f(_)

  val empty: String => Boolean = _ == ""
  val notEmpty = neg(empty)

  def isEven: (Int) => Boolean = x => x%2==0
  val isOdd = neg(isEven)
  val evenNumber = 2
