package u02esLab

object Es5 extends App:
  // 5. Composing functions
  def compose_1(f: Int => Int, g: Int => Int)(x: Int): Int = f(g(x))

  def compose_2(f: Int => Int, g: Int => Int): Int => Int =
    (a: Int) => f(g(a))

  def compose_3[X,Y,Z](f: Y => Z, g: X => Y): X => Z =
    (a: X) => f(g(a))

  def auxiliaryCompose(f: Double => Double): Double => Double = f(_)

  println("compose_2 " +  compose_2(_+1,_*5)(5))

  // Nella compose_3 il tipo generico porta ad interpretare i segni utilizzati come operatori matematici per
  // le lambda
  // println("compose_3 " + compose_3(_+1,y*5)(5)) // da errore

  println("compose_3 " + compose_3(auxiliaryCompose(_+1),auxiliaryCompose(_*5))(5))


